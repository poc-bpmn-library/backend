﻿namespace POC.Domain.Dtos
{
    public class GetFileResponseDto
    {
        public string FileName { get; set; }

        public byte[] BpmnDiagramFile { get; set; }
    }
}
