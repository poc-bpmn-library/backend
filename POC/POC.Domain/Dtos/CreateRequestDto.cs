﻿namespace POC.Domain.Dtos
{
    public class CreateRequestDto
    {
        public string XmlContent { get; set; }

        public string FileName { get; set; }

        public int AuthorId { get; set; }
    }
}
