﻿namespace POC.Domain.Dtos
{
    public class UpdateRequestDto
    {
        public string XmlContent { get; set; }

        public int AuthorId { get; set; }
    }
}
