﻿namespace POC.Domain.Dtos
{
    public class GetMetadataResponseDto
    {
        public Guid FileId { get; set; }

        public string FileName { get; set; }

        public int AuthorId { get; set; }

        public DateTime UploadDate { get; set; }
    }
}
