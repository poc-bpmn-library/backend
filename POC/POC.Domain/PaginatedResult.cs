﻿namespace POC.Domain
{
    public class PaginatedResult<T> where T : class
    {
        public List<T> Items { get; set; }

        public int PageIndex { get; private set; }

        public int PageSize { get; private set; }

        public PaginatedResult(IEnumerable<T> items, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
            Items = items.ToList();
        }
    }
}
