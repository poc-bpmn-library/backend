﻿using POC.Domain.Dtos;

namespace POC.Domain.Abstractions
{
    public interface IRepository
    {
        Task CreateAsync(CreateRequestDto request);
        Task<bool> DeleteAsync(Guid id);
        Task<bool> UpdateAsync(Guid id, UpdateRequestDto request);
        Task<GetFileResponseDto> GetByIdAsync(Guid id);
        Task<PaginatedResult<GetMetadataResponseDto>> GetPageAsync(int pageIndex, int pageSize);
    }
}
