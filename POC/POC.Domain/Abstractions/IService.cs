﻿using POC.Domain.Dtos;

namespace POC.Domain.Abstractions
{
    public interface IService
    {
        Task CreateAsync(CreateRequestDto request);
        Task<bool> DeleteAsync(Guid id);
        Task<bool> UpdateAsync(Guid id, UpdateRequestDto request);
        Task<GetFileResponseDto> GetFileByIdAsync(Guid id);
        Task<PaginatedResult<GetMetadataResponseDto>> GetMetadataPageAsync(int pageIndex, int pageSize);
    }
}
