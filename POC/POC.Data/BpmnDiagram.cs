﻿using System.ComponentModel.DataAnnotations;

namespace POC.Data
{
    public class BpmnDiagram
    {
        public Guid FileId { get; set; }

        public string FileName { get; set; }

        [Required]
        public string FilePath { get; set; }

        public int AuthorId { get; set; }

        public DateTime UploadDate { get; set; }

    }
}
