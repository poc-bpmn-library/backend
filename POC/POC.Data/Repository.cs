﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using POC.Domain;
using POC.Domain.Abstractions;
using POC.Domain.Dtos;

namespace POC.Data
{
    public class Repository : IRepository
    {
        private readonly BpmnDbContext _dbContext;
        private readonly string _directoryPath;

        public Repository(BpmnDbContext dbContext, IOptions<PathToBpmnDirectory> pathOptions)
        {
            _dbContext = dbContext;
            _directoryPath = pathOptions.Value.Path;
        }

        public async Task CreateAsync(CreateRequestDto dto)
        {
            var fileName = dto.FileName + ".xml";
            var filePath = Path.Combine(_directoryPath, fileName);
            await File.WriteAllTextAsync(filePath, dto.XmlContent);

            var bpmnDiagram = new BpmnDiagram
            {
                FileName = fileName,
                AuthorId = dto.AuthorId,
                UploadDate = DateTime.Now,
                FilePath = filePath
            };

            _dbContext.BpmnDiagrams.Add(bpmnDiagram);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var entity = await _dbContext.BpmnDiagrams.FirstOrDefaultAsync(x => x.FileId == id);

            if(entity is null)
            {
                return false;
            }

            if(File.Exists(entity.FilePath))
            {
                File.Delete(entity.FilePath);
            }

            _dbContext.BpmnDiagrams.Remove(entity);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<GetFileResponseDto> GetByIdAsync(Guid id)
        {
            var entity = await _dbContext.BpmnDiagrams.FirstOrDefaultAsync(x => x.FileId == id);

            if (entity is null)
            {
                return null;
            }

            if (File.Exists(entity.FilePath))
            {
                var fileContent = await File.ReadAllBytesAsync(entity.FilePath);

                return new GetFileResponseDto { BpmnDiagramFile = fileContent, FileName = entity.FileName };
            }

            return null;
        }

        public async Task<bool> UpdateAsync(Guid id, UpdateRequestDto dto)
        {
            var entity = await _dbContext.BpmnDiagrams.FirstOrDefaultAsync(x => x.FileId == id);

            if (entity is null)
            {
                return false;
            }

            await File.WriteAllTextAsync(entity.FilePath, dto.XmlContent);

            entity.AuthorId = dto.AuthorId;
            entity.UploadDate = DateTime.Now;

            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<PaginatedResult<GetMetadataResponseDto>> GetPageAsync(int pageIndex, int pageSize)
        {
            var entities = await _dbContext.BpmnDiagrams.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();

            var items = entities.Select(e => new GetMetadataResponseDto
            {
                FileId = e.FileId,
                FileName = e.FileName,
                AuthorId = e.AuthorId,
                UploadDate = e.UploadDate
            }).ToList();

            return new PaginatedResult<GetMetadataResponseDto>(items, pageIndex, pageSize);
        }
    }
}