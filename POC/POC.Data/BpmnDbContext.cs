﻿using Microsoft.EntityFrameworkCore;

namespace POC.Data
{
    public class BpmnDbContext : DbContext
    {
        public BpmnDbContext(DbContextOptions<BpmnDbContext> options) : base(options) 
        {
        }

        public DbSet<BpmnDiagram> BpmnDiagrams { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BpmnDiagram>()
                .HasKey(e => e.FileId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
