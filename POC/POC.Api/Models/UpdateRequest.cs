﻿namespace POC.Api.Models
{
    public class UpdateRequest
    {
        public int AuthorId { get; set; }

        public IFormFile BpmnDiagramFile { get; set; }
    }
}
