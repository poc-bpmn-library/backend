﻿namespace POC.Api.Models
{
    public class CreateRequest
    {
        public string FileName { get; set; }

        public int AuthorId { get; set; }

        public IFormFile BpmnDiagramFile { get; set; }
    }
}
