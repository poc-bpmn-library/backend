﻿using Microsoft.AspNetCore.Mvc;
using POC.Api.Models;
using POC.Domain.Abstractions;
using POC.Domain.Dtos;

namespace POC.Api.Controllers
{
    [Route("api/bpmn-diagrams")]
    [ApiController]
    public class Controller : ControllerBase
    {
        private readonly IService _service;
        private readonly ILogger<Controller> _logger;

        public Controller(IService service, ILogger<Controller> logger)
        {
            _service = service;
            _logger = logger;
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetBpmnDiagramByIdAsync([FromRoute] Guid id)
        {
            try
            {
                var result = await _service.GetFileByIdAsync(id);

                if (result is null || result.BpmnDiagramFile is null)
                {
                    return NotFound();
                }

                return File(result.BpmnDiagramFile, "application/xml", result.FileName);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred.");
                return StatusCode(500, "An error occurred." + ex);
            }
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<GetMetadataResponseDto>>> GetMetadataForAllDiagramsAsync(
            [FromQuery] int pageIndex = 1,
            [FromQuery] int pageSize = 10)
        {
            try
            {
                var result = await _service.GetMetadataPageAsync(pageIndex, pageSize);

                return Ok(result.Items);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred.");
                return StatusCode(500, "An error occurred." + ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateBpmnDiagramAsync([FromForm] CreateRequest request)
        {
            try
            {
                string xmlContent;
                using (var reader = new StreamReader(request.BpmnDiagramFile.OpenReadStream()))
                {
                    xmlContent = await reader.ReadToEndAsync();
                }

                var dto = new CreateRequestDto
                {
                    FileName = request.FileName,
                    XmlContent = xmlContent,
                    AuthorId = request.AuthorId
                };

                await _service.CreateAsync(dto);

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred.");
                return StatusCode(500, "An error occurred." + ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBpmnDiagramAsync([FromRoute] Guid id)
        {
            try
            {
                var success = await _service.DeleteAsync(id);

                if (!success)
                {
                    return NotFound();
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred.");
                return StatusCode(500, "An error occurred." + ex);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateBpmnDiagramAsync([FromRoute] Guid id, [FromForm] UpdateRequest request)
        {
            try
            {
                string xmlContent;
                using (var reader = new StreamReader(request.BpmnDiagramFile.OpenReadStream()))
                {
                    xmlContent = await reader.ReadToEndAsync();
                }

                var dto = new UpdateRequestDto
                {
                    XmlContent = xmlContent,
                    AuthorId = request.AuthorId
                };

                var success = await _service.UpdateAsync(id, dto);

                if (!success)
                {
                    return NotFound();
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred.");
                return StatusCode(500, "An error occurred." + ex);
            }
        }
    }
}
