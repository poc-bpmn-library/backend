using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using POC.Data;
using POC.Domain;
using POC.Domain.Abstractions;
using POC.Services;

var AllowReactOrigin = "_allowReactOrigin";

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: AllowReactOrigin,
                      policy =>
                      {
                          policy.WithOrigins("http://localhost:5173")
                                .AllowAnyHeader()
                                .AllowAnyMethod();
                      });
});

builder.Services.Configure<PathToBpmnDirectory>(builder.Configuration.GetSection("PathToBpmnDirectory"));

builder.Services.AddControllers();

builder.Services.AddDbContext<BpmnDbContext>(options =>
{
    var connectionStringsSection = builder.Configuration.GetSection("ConnectionStrings");
    options.UseSqlServer(connectionStringsSection["Default"]);
});

builder.Services.AddScoped<IService, Service>();

builder.Services.AddScoped<IRepository, Repository>();

builder.Services.AddLogging();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors(AllowReactOrigin);

app.UseAuthorization();

app.MapControllers();

app.Run();
