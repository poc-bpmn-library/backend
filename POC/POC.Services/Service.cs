﻿using POC.Domain;
using POC.Domain.Abstractions;
using POC.Domain.Dtos;

namespace POC.Services
{
    public class Service : IService
    {
        private readonly IRepository _repository;

        public Service(IRepository repository)
        {
            _repository = repository;
        }

        public async Task CreateAsync(CreateRequestDto request)
        {
            await _repository.CreateAsync(request);
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            return await _repository.DeleteAsync(id);
        }

        public async Task<GetFileResponseDto> GetFileByIdAsync(Guid id)
        {
            return await _repository.GetByIdAsync(id);
        }

        public async Task<PaginatedResult<GetMetadataResponseDto>> GetMetadataPageAsync(int pageIndex, int pageSize)
        {
            return await _repository.GetPageAsync(pageIndex, pageSize);
        }

        public async Task<bool> UpdateAsync(Guid id, UpdateRequestDto request)
        {
            return await _repository.UpdateAsync(id, request);
        }
    }
}